# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.1.0 (2021-06-24)


### Features

* add standard release support! ([9c4bdb1](https://gitlab.com/krishwar2001/my-first-project/commit/9c4bdb1e44727f59a438e6fa8322484264c9faa5))
